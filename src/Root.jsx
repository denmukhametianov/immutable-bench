import React from 'react';
import ContentBlock from '../containers/ContentBlock/ContentBlock';
import Header from '../containers/Header/Header.jsx';
import {Gapped, Input, Button, Spinner, Center} from 'ui';
import BenchBlock from './BenchBlock';
import Immutable from 'immutable';

export default class Root extends React.Component {
    state = {
        elements: '',
    };

    render() {
        const self = this;
        return <div>
            <Header>Header</Header>
            <ContentBlock>
                <ContentBlock.Body>
                    <Center>
                        <ContentBlock.Title>
                            Immutable benchmarks
                        </ContentBlock.Title>
                        <ContentBlock.Value>
                            <Gapped vertical gap={10}>
                                <div>
                                    <Gapped gap={10}>
                                        <div>Количество элементов: <Input value={this.state.elements} maxLength={8}
                                                                          onChange={(e, v) => self._onInputValue('elements', v)}/>
                                        </div>
                                    </Gapped>
                                </div>
                                <table>
                                    <tbody>
                                    <BenchBlock
                                        name="Array, naive"
                                        init={x => self._fillArray(x)}
                                        equals={(a, b) => self._arraysEqual(a, b)}
                                        elements={this.state.elements}
                                    />
                                    <BenchBlock
                                        name="Array, ==="
                                        init={x => self._fillArray(x)}
                                        elements={this.state.elements}
                                        equals={(a, b) => a === b}
                                    />
                                    <BenchBlock
                                        name="Immutable.List, ==="
                                        init={x => self._fillList(x)}
                                        elements={this.state.elements}
                                        equals={(a, b) => a === b}
                                    />
                                    <BenchBlock
                                        name="Immutable.List, naive"
                                        init={x => self._fillList(x)}
                                        equals={(a, b) => self._listEqual(a, b)}
                                        elements={this.state.elements}
                                    />
                                    <BenchBlock
                                        name="Immutable.List, equals"
                                        init={x => self._fillList(x)}
                                        equals={(a, b) => a.equals(b)}
                                        elements={this.state.elements}
                                    />
                                    </tbody>
                                </table>
                            </Gapped>
                        </ContentBlock.Value>
                    </Center>
                </ContentBlock.Body>
            </ContentBlock>
        </div>;
    }

    _onInputValue(name, value) {
        var parsed = parseInt(value);
        if (value !== '' && (isNaN(parsed) || parsed === undefined || parsed === null)) {
            return;
        }
        this.setState({
            [name]: value === '' ? value : String(parsed),
        })
    }

    _listEqual(a, b) {
        if (a.size !== b.size) {
            return false;
        }
        for (let i = 0; i < a.size; i++) {
            if (a.get(i) !== b.get(i))
                return false;
        }
        return true;
    }

    _arraysEqual(a, b) {
        if (a.length !== b.length) {
            return false;
        }
        for (let i = 0; i < a.length; i++) {
            if (a[i] !== b[i])
                return false;
        }
        return true;
    }

    _fillList(count) {
        return Immutable.List(this._fillArray(count));
    }

    _fillArray(count) {
        var result = [];
        for (let i = 0; i < count; i++) {
            result[i] = i;
        }
        return result;
    }
}
