import React from 'react';
import ContentBlock from '../containers/ContentBlock/ContentBlock';
import Header from '../containers/Header/Header.jsx';
import {Gapped, Input, Button, Spinner} from 'ui';
import Immutable from 'immutable';

export default class BenchBlock extends React.Component {
    state = {
        result: null,
    };

    render() {
        const {name, init, equals, elements} = this.props;
        const iterations = 100;
        return (
            <tr>
                <td width="150">{name}</td>
                <td>
                    <Button use="primary" onClick={() => this._measure(init, equals, elements, iterations)}
                            disabled={elements === '' || iterations === ''}>
                        Запустить!
                    </Button>
                </td>
                <td width="150">
                    {this.state.loading && <Spinner type="mini" caption=""/>}
                    {this.state.result !== null && ((this.state.result / iterations).toFixed(3) + ' ms')}
                </td>
            </tr>
        );
    }

    _measure(init, equals, elements, iterations) {
        this.setState({
            loading: true,
            result: null,
        });
        var self = this;
        setTimeout(() => {
            const a = init(elements);
            const b = init(elements);
            const start = performance.now();
            for(let i = 0; i < iterations; i++) {
                equals(a, b);
            }
            const end = performance.now();
            self.setState({
                loading: false,
                result: end - start,
            });
        }, 500);

    }
}
