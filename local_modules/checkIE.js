export default function isIE() {
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf('MSIE ');
  var trident = ua.indexOf('Trident/');
  var edge = ua.indexOf('Edge/');
  if (msie > 0) {
    return true;
  }
  else if (trident > 0) {
    return true;
  }
  else if (edge > 0) {
    return true;
  }
  else
    return false;
}
