import React from 'react';
import ReactDom from 'react-dom';

import Root from './src/Root.jsx';

import styles from './app.less';

ReactDom.render(
    <Root/>,
    document.getElementById('content')
);
