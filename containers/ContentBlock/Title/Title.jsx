import React, {PropTypes} from 'react';
import styles from '../ContentBlock.less'

export default function Title(props) {
    return <div className={styles.title}>{props.children}</div>;
}
