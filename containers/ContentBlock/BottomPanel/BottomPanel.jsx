import React from 'react';
import styles from '../ContentBlock.less';
import {Gapped} from 'ui';

export default function BottomPanel(props) {
    return <div className={styles.bottomPanel}><Gapped gap={10}>{props.children}</Gapped></div>
}
