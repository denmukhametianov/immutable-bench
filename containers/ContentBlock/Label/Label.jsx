import React from 'react';
import styles from '../ContentBlock.less';

export default function Label(props) {
    return <span className={styles.label} style={{width:props.width || 241}}>{props.children}</span>
}

