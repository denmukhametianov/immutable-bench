import React from 'react'

export default function Logo({fontSize, color, style, ...props}) {
    return <span style={{ ...style, fontSize: fontSize}} {...props}>
    к
            <span>
                <svg x="0px" y="0px" viewBox="423.4 585.2 36 26" height="0.8em" width="1.1em"
                     style={{ position: 'relative', top: '0.05em' }}>
                    <path fill={color}
                          d="M440.3,587.8c2.8,0,5.4,1.3,7.1,3.5l0.7,1h1.2c4.3,0.3,7.6,3.8,7.6,8.1c0,4.5-3.6,8.1-8.2,8.1h-15.8  c-3.8,0-6.9-3.1-6.9-7c0-3,1.8-5.5,4.6-6.6l1.2-0.3l0.5-1.2C433.4,590.2,436.7,587.8,440.3,587.8 M440.3,585.2  c-4.9,0-9,3.1-10.7,7.5c-3.6,1.3-6.2,5-6.2,8.9c0,5.3,4.3,9.6,9.5,9.6h15.8c5.9,0,10.7-4.8,10.7-10.8c0-5.8-4.4-10.4-10-10.8  C447.4,587,444.1,585.2,440.3,585.2L440.3,585.2z"/>
                </svg>
            </span>
    нтур
</span>
}


